# ResponseTimes

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/response_times`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

This app is only intended to be run from the command line on your system.

Please also have the following tools installed globally:

* bundle >= 1.13.6
* rake >= 10.5.0

To install: 

1. pull the repository
2. run `bundle exec rake install` inside the top level of the repository
3. run `bundle exec response_times` from the command line

## Usage

Once installed you can run:

`response_times <seconds_per_increment> <length_of_time_to_run_in_seconds>`

For example, say you wanted to run it for *Five Minutes* every 10 seconds and output it to a CSV file, you would run:

`response_times 10 300 > output.csv`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/zpallin/response-times. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

