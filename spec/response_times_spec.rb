require 'spec_helper'

describe ResponseTimes do
  it 'has a version number' do
    expect(ResponseTimes::VERSION).not_to be nil
  end

  describe ResponseTimes::Counter do
    it 'has csv headers to output' do
      counter = ResponseTimes::Counter.new
      expect(counter.headers).to eq('increment, current_time, average')
    end

    it 'can output text in a csv line' do
      expected = '1, 10, 10.000000'
      counter = ResponseTimes::Counter.new
      expect(counter.output(1, 10, 10)).to eq(expected)
    end

    it 'can get a response time in seconds to gitlab.com' do
      counter = ResponseTimes::Counter.new
      expect(counter.response_time).to be_a_kind_of(Float) # should be float
      expect(counter.response_time).to_not eq(0) # should be more than 0
    end

    it 'can run with specific intervals in seconds over seconds length' do
      counter = ResponseTimes::Counter.new
      expect(counter).to receive(:sleep)
      allow(counter).to receive(:get_response_time)
      allow(counter).to receive(:puts)
      counter.count(1, 2)

      expect(counter).not_to receive(:sleep)
      counter.count(1, 1)
    end
  end
end
