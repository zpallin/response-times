require 'response_times/version'
require 'net/http'
require 'benchmark'

module ResponseTimes
  # a module designed for collecting response data from interacting
  # with https://gitlab.com

  # counter class
  class Counter
    # an object used to facilitate the counting of incrememntal
    # response time checks and output a csv

    def headers
      'increment, current_time, average'
    end

    def output(inc, time, average)
      # cleanly output input report
      "#{inc}, #{time}, #{format('%.6f', average)}"
    end

    def response_time
      # how we actually get the response time
      Benchmark.measure do
        Net::HTTP.get('gitlab.com', '/')
      end.to_s.tr('()', '').split(' ').last.to_f
    end

    def count(int, len)
      # loops over each response, generates csv row
      total_time = 0
      begin
        puts headers
        1.step do |inc|
          time = response_time
          total_time += time.to_f
          puts output(inc, time, total_time / inc)
          break if inc >= len.to_i / int.to_i
          sleep(int.to_i)
        end
      rescue SystemExit, Interrupt
        puts 'Exiting...'
      end
    end
  end
end
